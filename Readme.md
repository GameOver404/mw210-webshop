# MW210 Webshop

This webshop was created with Django-Oscar.

## Prerequisites

This project requires the following dependencies to be installed beforehand:

* Python and pip  
* Java

## Dependencies

* Django-Oscar
* Sorl-Thumbnail
* Pycountry
* Pysolr

To install all the needed dependencies run:

```
pip install -r requirements.txt
```

## Usage

* Once the dependencies are installed (via pip) start the app with:

```
python manage.py runserver
```


* Use the following command in the Solr folder to start the optimized search with Apache Solr:

```
java -jar start.jar
```

# Gifs

## Anmeldung

![](gifs/Anmeldung.gif)

## Produktauswahl und Warenkorb

![IMAGE_DESCRIPTION](gifs/Produktauswahl+Warenkorb.gif)

## Gutschein erhalten

![](gifs/Gutschein_erfüllt.gif)

## Kundenbewertungen
![](gifs/Kundenbewertungen.gif)
