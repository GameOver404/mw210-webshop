"""MW210 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.apps import apps
from django.urls import include, path
from django.contrib import admin
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from . import settings
from django.conf.urls import url
from django.contrib.auth import logout

# These need to be imported into this namespace
from oscar.views import handler500, handler404, handler403  # noqa

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),

    # The Django admin is not officially supported; expect breakage.
    # Nonetheless, it's often useful for debugging.

    path('admin/', admin.site.urls),

    # Django-Oscar
    path('', include(apps.get_app_config('oscar').urls[0])),


    # Rest API
    path("api/", include("oscarapi.urls")),

    # About app
    url(r'^about/', include('apps.about.urls')),

    # PayPal extension
    url(r'^checkout/paypal/', include('paypal.express.urls')),
    path('', include('social_django.urls', namespace='social')),
    path('logout/', logout, {'next_page': settings.LOGOUT_REDIRECT_URL},
         name='logout'),

]

# Prefix Oscar URLs with language codes
# urlpatterns += i18n_patterns(
#     path('', include(apps.get_app_config('oscar').urls[0])),)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    # urlpatterns += static(settings.STATIC_URL,
    #                       document_root=settings.STATIC_ROOT)
