#!/usr/bin/env bash

echo "===> delete db file"
rm -rf db.sqlite

echo "===> syncdb & migrate"
python manage.py makemigrations
python manage.py migrate

echo "===> update index"
python manage.py clear_index --noinput
python manage.py update_index catalogue
