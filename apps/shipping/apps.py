from django.apps import AppConfig


class ShippingConfig(AppConfig):
    name = 'apps.shipping'


# def test_returns_correct_weight_for_nonempty_basket(self):
#     basket = factories.create_basket(empty=True)
#     products = [
#         factories.create_product(attributes={'weight': '1'},
#                                  price=D('5.00')),
#         factories.create_product(attributes={'weight': '2'},
#                                  price=D('5.00'))]
#     for product in products:
#         basket.add(product)

#     scale = Scale(attribute_code='weight')
#     self.assertEqual(1 + 2, scale.weigh_basket(basket))
