from decimal import Decimal as D

from oscar.apps.shipping.methods import *
from django.template.loader import render_to_string

from oscar.core import prices


class Standard(methods.Base):
    code = 'standard'
    name = 'Standard shipping'
    charge_excl_tax = D('6.95')
    charge_incl_tax = D('6.95')

    def calculate(self, basket):
        # Free for orders over some threshold
        if basket.total_incl_tax > self.threshold:
            return prices.Price(
                currency=basket.currency,
                excl_tax=D('0.00'),
                incl_tax=D('0.00'))


class Express(methods.Base):
    code = 'express'
    name = 'Express shipping'

    charge_per_item = D('1.50')
    description = render_to_string(
        'shipping/express.html', {'charge_per_item': charge_per_item})

    def calculate(self, basket):
        total = basket.num_items * self.charge_per_item
        return prices.Price(
            currency=basket.currency,
            excl_tax=total,
            incl_tax=total)
