from oscar.apps.shipping.repository import Repository as OscarRepository

from . import methods

# Override free shipping methods


class Repository(repository.Repository):
    methods = (methods.Standard(), methods.Express())
